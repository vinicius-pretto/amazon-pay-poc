const express = require("express");
const path = require("path");
const fs = require("fs");
const config = require("../config");
const AmazonPayService = require("./amazonPay/amazonPayService");

const app = express();
const amazonPayService = new AmazonPayService();

app.use(express.json());
app.use(
  express.static(path.resolve(config.staticFolder), config.staticOptions)
);

app.get("/address", async (req, res) => {
  try {
    const { accessToken, orderReferenceId } = req.query;
    const response = await amazonPayService.getOrderReferenceDetails(accessToken, orderReferenceId);
    res.json(response);
  } catch (error) {
    const errorMessage = error.response && error.response.data || error.message;
    console.log(errorMessage);
    res.sendStatus(500);
  }
});

app.get("/amazonPay", async (req, res) => {
  console.log('/amazonPay')
  try {
    const accessToken = await amazonPayService.getAccessToken(req.query.code);
    const profile = await amazonPayService.retrieveProfile(accessToken);
    const pfxJoinUrl = req.query.state;
    console.log("==========> profile    :", profile);
    res.cookie("amazon_Login_accessToken", accessToken);
    res.cookie("access_token", accessToken);
    res.redirect(pfxJoinUrl);
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  }
});

app.get("/", (req, res) => {
  const indexFilePath = path.resolve("public/index.html");
  const encodeType = "utf-8";

  fs.readFile(indexFilePath, encodeType, (error, content) => {
    if (error) {
      return res.sendStatus(500);
    } else {
      const indexHtml = content
        .replace("%CLIENT_ID%", config.amazonPay.clientId)
        .replace("%MERCHANT_ID%", config.amazonPay.merchantId);

      return res.send(indexHtml);
    }
  });
});

module.exports = app;
