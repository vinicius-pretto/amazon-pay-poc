const axios = require("axios");
const qs = require("qs");
const crypto = require("crypto");
const _ = require('lodash');
const config = require("../../config");

class AmazonPayService {
  async getAccessToken(code) {
    const options = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };

    const body = qs.stringify({
      grant_type: "authorization_code",
      code: code,
      client_id: config.amazonPay.clientId,
      client_secret: config.amazonPay.clientSecret,
      redirect_uri: config.amazonPay.redirectUri
    });

    const response = await axios.post(
      `${config.amazonPay.host}/auth/o2/token`,
      body,
      options
    );
    return response.data.access_token;
  }

  async retrieveProfile(accessToken) {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };
    const response = await axios.get(
      `${config.amazonPay.host}/user/profile`,
      options
    );
    return response.data;
  }

  calculateStringToSignV2(parameters, path, serviceUrl) {
    var sorted = _.reduce(_.keys(parameters).sort(), function (m, k) {
      m[k] = parameters[k];
      return m;
    }, {});
    
    const params = qs.stringify(sorted);
    const endpoint = new URL(serviceUrl);
    const stringToSign = `POST\n${endpoint.host}\n${path}\n${params}`;
    return stringToSign;
  }

  urlEncode(rawValue) {
    return rawValue
      .replace(/\+/g, "%20")
      .replace(/\*/g, "%2A")
      .replace(/\%7E/g, "~");
  }

  sign(stringToSign, secretKey) {
    const signatureBase64 = crypto
      .createHmac("sha256", secretKey)
      .update(stringToSign, 'utf8')
      .digest("base64");

    return this.urlEncode(signatureBase64);
  }

  async getOrderReferenceDetails(accessToken, orderReferenceId) {
    const path = "/OffAmazonPayments_Sandbox/2013-01-01";
    const params = {
      AWSAccessKeyId: config.amazonPay.awsAccessKeyId,
      Action: "GetOrderReferenceDetails",
      SellerId: config.amazonPay.merchantId,
      MWSAuthToken: config.amazonPay.awsAuthToken,
      SignatureVersion: 2,
      Timestamp: new Date(),
      Version: '2013-01-01',
      SignatureMethod: 'HmacSHA256',
      AmazonOrderReferenceId: orderReferenceId,
      AddressConsentToken: accessToken
    };

    const formattedParameters = this.calculateStringToSignV2(
      params,
      path,
      config.amazonPay.mwsServiceHost
    );

    const signature = this.sign(formattedParameters, config.amazonPay.awsSecretKey);
    const body = qs.stringify({
      ...params,
      Signature: signature
    });

    const options = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };

    const response = await axios.post(
      `${config.amazonPay.mwsServiceHost}${path}`,
      body,
      options
    );
    return response.data;
  }
}

module.exports = AmazonPayService;
