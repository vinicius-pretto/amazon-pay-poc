const config = {
  port: process.env.PORT || 8080,
  staticFolder: process.env.STATIC_FOLDER || 'public',
  staticOptions: {
    index: false
  },
  amazonPay: {
    host: 'https://api.amazon.com',
    mwsServiceHost: 'https://mws.amazonservices.com',
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    redirectUri: process.env.REDIRECT_URI,
    merchantId: process.env.MERCHANT_ID,
    awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
    awsSecretKey: process.env.AWS_SECRET_KEY,
    awsAuthToken: process.env.AWS_AUTH_TOKEN
  }
};

module.exports = config;
