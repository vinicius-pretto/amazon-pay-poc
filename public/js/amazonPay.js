window.onAmazonLoginReady = onAmazonLoginReady;
window.onAmazonPaymentsReady = onAmazonPaymentsReady;

$("#AmazonLoginButton").addEventListener("click", onAuthorize);
$("#Logout").addEventListener("click", onLogout);

function $(element) {
  return document.querySelector(element);
}

function onAmazonLoginReady() {
  amazon.Login.setClientId(window.config.clientId);
  amazon.Login.setUseCookie(true);
  const accessToken = getCookie('access_token');

  if (accessToken) {
    addAddressWidget(billingAgreementId => {
      getAddress(billingAgreementId, accessToken);
      addWalletWidget(billingAgreementId);
    });
  }
}

function getCookie(name) {
  var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
  return v ? v[2] : null;
}

function onAmazonPaymentsReady() {
  showPaymentButton();
}

function getAddress(billingAgreementId, accessToken) {
  fetch(`http://localhost:8080/address?accessToken=${accessToken}&orderReferenceId=${billingAgreementId}`)
    .then(response => {
      console.log(response.body);
    })
    .catch(error => {
      console.log(error)
    });
}

function addAddressWidget(callback) {
  new OffAmazonPayments.Widgets.AddressBook({
    sellerId: window.config.merchantId,
    agreementType: 'OrderReference',

    onOrderReferenceCreate: function(orderReference) {
      const referenceId = orderReference.getAmazonOrderReferenceId();
      console.log('referenceId', referenceId);
    },

    onReady: function(billingAgreement) {
      console.log('addressWidget - billingAgreement', billingAgreement);
      var billingAgreementId = billingAgreement.getAmazonBillingAgreementId();
      callback(billingAgreementId);
    },

    onAddressSelect: (billingAgreement) => {
      console.log('onAddressSelect', billingAgreement);
    },

    design: {
      designMode: "responsive"
    },

    onError: error => {
      console.error("Error on add address widget", error);
    }
  }).bind("addressWidget");
}

function addWalletWidget(amazonBillingAgreementId) {
  console.log('amazonBillingAgreementId', amazonBillingAgreementId);

  new OffAmazonPayments.Widgets.Wallet({
    sellerId: window.config.merchantId,
    amazonBillingAgreementId: amazonBillingAgreementId,
    onPaymentSelect: function(billingAgreement) {
      console.log('onPaymentSelect - billingAgreement', billingAgreement);
    },
    design: {
      designMode: 'responsive'
    },
    onError: function(error) {
      // your error handling code
    }
  }).bind("walletWidget");
}

function showPaymentButton() {
  const amazonPayButtonOptions = {
    type: "PwA",
    color: "LightGray",
    size: "medium",
    authorization: onAuthorize,
    onError: error => {
      const errorCode = error.getErrorCode();
      const errorMessage = error.getErrorMessage();
      alert(`The following error occurred: ${errorCode} ${errorMessage}`);
    }
  };
  OffAmazonPayments.Button(
    "AmazonPayButton",
    window.config.merchantId,
    amazonPayButtonOptions
  );
}

function onAuthorize() {
  const callbackUrl = `${window.location.origin}/amazonPay`;
  loginOptions = {
    scope: "profile payments:widget payments:shipping_address postal_code",
    popup: false,
    response_type: "code",
    state: '/'
  };
  amazon.Login.authorize(loginOptions, callbackUrl);
}

function onLogout() {
  const state = {};
  const title = ""; 
  amazon.Login.logout();
  history.pushState(state, title, "/");
}
