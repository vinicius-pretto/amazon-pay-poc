# Amazon Pay POC

## References:

* [Login with Amazon Pay](https://developer.amazon.com/docs/login-with-amazon/web-docs.html)

## Running App

* Create `.env` file with the following variables

```
CLIENT_ID=
CLIENT_SECRET=
REDIRECT_URI=
MERCHANT_ID=
```

* Install dependencies

```
$ npm install
```

* Start

```
$ npm start
```

## LWA(Login with Amazon Pay)

Steps:

* [Step 1: Register for LWA](https://developer.amazon.com/docs/login-with-amazon/register-web.html)
* [Step 2: Add a LWA Button to yout website](https://developer.amazon.com/docs/login-with-amazon/add-a-button-web.html)
* [Step 3: Add the LWA SDK for javascript](https://developer.amazon.com/docs/login-with-amazon/install-sdk-javascript.html)
* [Step 4: Choose an authorization Grunt](https://developer.amazon.com/docs/login-with-amazon/choose-authorization-grant.html)
* [Step 5: Dynamically Redirect Users](https://developer.amazon.com/docs/login-with-amazon/dynamically-redirect-users.html)
* [Step 6: Obtain Customer Profile Info](https://developer.amazon.com/docs/login-with-amazon/obtain-customer-profile.html)
* [Step 7: Log Out Users](https://developer.amazon.com/docs/login-with-amazon/log-out-users.html)
* [Step 8: Integrate with Your Account](https://developer.amazon.com/docs/login-with-amazon/integrate-with-account-system-websites.html)